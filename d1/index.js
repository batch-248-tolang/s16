console.log("Hello world")

// comment + ctrl /

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = y - x;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let modulo = x % y;
console.log("Result of modulo operator: " + modulo);

// Assignment Operator

let assignmentNumber = 8;

// Addition assignment operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Shorthand assignment operator
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parantheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);
// 3*4 = 12
// 12/5 = 2.4
// 1+2 = 3
// 3-2.4 = 0.6

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);

// 4/5 = 0.8
// 2-3 = -1
// 0.8*(-1) = -0.8
// -0.8+1 = 0.2


// Increment and Decrement
// Operators that add or subtract value by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// pre-increment will add and then reassign

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
let increment = ++z;
console.log("Result of pre-increment: " + increment);
// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
console.log("Result of pre-increment: " + z);

// post-increment will reassign and then add

// The value of "z" is returned and stored in the variable increment then the value of "z" is increased by one
increment = z++;
// The value of "z" is at 2 before it was incremented
console.log("Result of post-increment: " + increment);
// The value of "z" was increased again, reassigning the value to 3
console.log("Result of post-increment: " + z);

// let a = 2;
// let increment1 = a++;
// console.log(increment1)

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


// Type coercion

let numA = "10";
let numB = 12;
let coercion = numA + numB;
console.log(coercion)
console.log(typeof coercion)

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

let numE = true + 1;
console.log(numE)

let numF = false + 1;
console.log(numF)

// Comparison Operators

let juan = "juan";//assignment operator

// Equality operator (==)

console.log(1==1);
console.log(1==2);
console.log(1=="1");
console.log(0==false);
console.log("juan"=="juan")
console.log("juan"==juan)

// Strict equality operator

console.log(1===1);
console.log(1===2);
console.log(1==="1");
console.log(0===false);
console.log("juan"==="juan")
console.log("juan"===juan)

// Inequality operator (==)

console.log(1!=1);
console.log(1!=2);
console.log(1!="1");
console.log(0!=false);
console.log("juan"!="juan")
console.log("juan"!=juan)

// Strict inequality operator

console.log(1!==1);
console.log(1!==2);
console.log(1!=="1");
console.log(0!==false);
console.log("juan"!=="juan")
console.log("juan"!==juan)

// Relational Operators

let a = 50;
let b = 65;

let isGreaterThan = a>b;
let isLessThan = a<b
let isGTorEqual1 = a >= b;
let isLTorEqual1 = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual1);
console.log(isLTorEqual1);

// forced coercion to change the string into a nunber
let numStr = "30";
console.log(a>numStr);
console.log(b<=numStr);


// Logical Operators

let isLegalAge = true;
let isRegistered = false;
let isMarried = true;

// Logical And Operator (&&)
let allRequirementsMet = isLegalAge && isRegistered;
let example = isLegalAge && isMarried;
console.log("Result of Logical and operator: " + allRequirementsMet);


// Logical Or Operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical Not Operator (!)
let someRequirementsNotMet = !isRegistered;

// Sample
let num5 = 23000;
let sampleRemainder = num5%5;
console.log("The remainder of " + num5 + "divided by 5 is: " + sampleRemainder);
let isDivisibleBy5 = sampleRemainder === 0
console.log("Is num5 divisible by 5?");
console.log(isDivisibleBy5)